import os

WEBSOCKET_URL = os.environ.get('WEBSOCKET_URL', "ws://128.199.107.120:11011")
# WEBSOCKET_URL = os.environ.get('WEBSOCKET_URL', "ws://api.dxpchain-kibana.info/ws")


# Default connection to Elastic Search.
ELASTICSEARCH = {
     'hosts': os.environ.get('ELASTICSEARCH_URL', 'http://kibana.gvxlive.com').split(','),
     #'hosts': os.environ.get('ELASTICSEARCH_URL', 'https://DxpChain:Infrastructure@eu.elasticsearch.dxpchain.ws:443').split(','),
     'user': os.environ.get('ELASTICSEARCH_USER', 'kibanaadmin'),
     'password': os.environ.get('ELASTICSEARCH_PASS', '1N4W#Y8#Bn4@')
}


# Optional ElasticSearch cluster to access other data.
# Currently expect:
#   - 'operations': for dxpchain-* indexes where operations are stored
#   - 'objects': for objects-* indexes where Chain data is stored.
#
# Sample:
#
# ELASTICSEARCH_ADDITIONAL {
#   'operations': None, # Use default cluster.
#   'objects': {
#     'hosts': ['https://es.mycompany.com/'],
#     'user': 'myself',
#     'password': 'secret'
#    }
# }
ELASTICSEARCH_ADDITIONAL = {
    # Overwrite cluster to use to retrieve dxpchain-* index.
    'operations': None,
    # Overwrite cluster to use to retrieve dxpchain-* index.
    'objects': {
        'hosts': ['http://kibana.gvxlive.com'] # oxarbitrage (no credentials)
        #'hosts': ['https://DxpChain:Infrastructure@eu.elasticsearch.dxpchain.ws:443'] # infra
    }

}

# Cache: see https://flask-caching.readthedocs.io/en/latest/#configuring-flask-caching
CACHE = {
    'CACHE_TYPE': os.environ.get('CACHE_TYPE', 'simple'), # use 'uwsgi' when running under uWSGI server.
    'CACHE_DEFAULT_TIMEOUT': int(os.environ.get('CACHE_DEFAULT_TIMEOUT', 600)) # 10 min
}

# Configure profiler: see https://github.com/muatik/flask-profiler
PROFILER = {
    'enabled': os.environ.get('PROFILER_ENABLED', False),
    'username': os.environ.get('PROFILER_USERNAME', "bytemater"),
    'password': os.environ.get('PROFILER_PASSWORD', "supersecret"),
}

CORE_TOKEN_SYMBOL = 'DXP'
CORE_TOKEN_ID = '1.3.0'

TESTNET = 0 # 0 = not in the testnet, 1 = testnet
CORE_TOKEN_SYMBOL_TESTNET = 'TEST'

# Choose which APIs to expose, default to all.
#EXPOSED_APIS = ['explorer', 'es_wrapper', 'udf']
