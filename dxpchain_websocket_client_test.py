import pytest

from services.dxpchain_websocket_client import DxpchainWebsocketClient
import config

@pytest.fixture
def dxpchain_client():
    return DxpchainWebsocketClient(config.WEBSOCKET_URL)

def test_ws_request(dxpchain_client):
    response = dxpchain_client.request('database', "get_dynamic_global_properties", [])
    assert response['id'] == '2.1.0'
    assert 'head_block_number' in response

def test_automatic_api_id_retrieval(dxpchain_client):
    dxpchain_client.request('asset', 'get_asset_holders', ['1.3.0', 0, 100])

def test_get_object(dxpchain_client):
    object = dxpchain_client.get_object('1.3.0')
    assert object